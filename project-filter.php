<?php
/**
 * Plugin Name: Project Filter
 * Description: Plugin for filtering Projects by Platforms and Languages
 * Author: Tetyana Trukhina (QArea)
 * Author URI: qarea.com
 * Text Domain: project-filter
 * Domain Path:
 * Version: 0.1.0
 */

class Project_Filter_Plugin {

  public function __construct() {
    add_action( 'init', array( $this, 'create_project_taxonomy' ) );
  }

  public static function get_initial_terms() {
    return get_terms( [
      'taxonomy'   => array( 'case_platforms', 'case_languages' ),
      'hide_empty' => false,
      'fields'     => 'id=>slug',
    ] );
  }

  public static function create_project_taxonomy() {
    if ( ! taxonomy_exists( 'project' ) ) {
      $project_labels = array(
        'name'          => _x( 'Projects', 'taxonomy general name' ),
        'singular_name' => _x( 'Project', 'taxonomy singular name' ),
        'search_items'  => __( 'Search Projects' ),
        'all_items'     => __( 'All Projects' ),
        'parent_item'   => __( 'Parent Project' ),
        'parent_item_colon' => ( 'Parent Project' ),
        'edit_item'     => __( 'Edit Project' ),
        'update_item'   => __( 'Update Project' ),
        'add_new_item'  => __( 'Add New Project' ),
        'new_item_name' => __( 'New Project Name' ),
        'menu_name'     => __( 'Projects' ),
      );

      $project_tax_args = array(
        'hierarchical' => true,
        'labels'       => $project_labels,
        'show_ui'      => false,
        'show_admin_column' => false,
        'query_var'    => true,
        'rewrite'      => array( 'slug' => 'project', 'with_front' => false ),
      );

      register_taxonomy( 'project', array( 'projects' ), $project_tax_args );
    }
  }

  public static function populate_project_tax() {
    $initial_terms = self::get_initial_terms();

    foreach ( $initial_terms as $key => $value ) {
      $insert_term_result = wp_insert_term( $value, 'project', array( 'slug' => $value ) );
      $has_project_term_meta = get_term_meta( $key, 'project_term_id', true );

      if ( ! is_wp_error( $insert_term_result ) ) {

        if ( $has_project_term_meta ) {
          update_term_meta( $key, 'project_term_id', $insert_term_result['term_id'], $has_project_term_meta );
        } else {
          add_term_meta( $key, 'project_term_id', $insert_term_result['term_id'], true );
        }

      } else {

        $project_term = get_term_by( 'slug', $value, 'project' );

        if ( $has_project_term_meta ) {
          update_term_meta( $key, 'project_term_id', $project_term->term_id );
        } else {
          add_term_meta( $key, 'project_term_id', $project_term->term_id, true );
        }
      }
    }
  }

  public static function add_project_terms_to_cases() {

    global $post;

    $args = array(
      'post_type'      => 'projects',
      'posts_per_page' => -1,
      'post_status'    => 'any'
    );

    $cases_query = new WP_Query( $args );

    if ( $cases_query->have_posts() ) {
      while ( $cases_query->have_posts() ) {

        $cases_query->the_post();
        self::set_case_project_terms( $post->ID, $post );
      }
    }
    wp_reset_postdata();
  }

  /**
   * @param $post_id
   * @param $post
   */
  public static function set_case_project_terms( $post_id, $post ) {
  	$languages_terms_arr = is_array( get_the_terms( $post_id, 'case_languages' ) ) ? get_the_terms( $post_id, 'case_languages' ) : [];
	$platforms_terms_arr = is_array( get_the_terms( $post_id, 'case_platforms' ) ) ? get_the_terms( $post_id, 'case_platforms' ) : [];

    $cases_post_terms = array_merge( $languages_terms_arr, $platforms_terms_arr );

    foreach ( $cases_post_terms as $cases_post_term ) {
      $cases_post_term_meta = get_term_meta( $cases_post_term->term_id, 'project_term_id', true );
      wp_set_post_terms( $post_id, [$cases_post_term_meta], 'project', true );
    }

    $case_project_terms = get_the_terms( $post_id, 'project' );
    if ( is_array( $case_project_terms ) && count( $case_project_terms ) > count( $cases_post_terms ) ) {
			foreach ( $case_project_terms as $case_project_term ) {
				if ( false == ( has_term( $case_project_term->slug, 'case_languages', $post ) || has_term( $case_project_term->slug, 'case_platforms', $post ) ) ) {
					wp_remove_object_terms( $post_id, $case_project_term->term_id, 'project' );
				}
			}
		}
  }

  public static function activate_project_filter_plugin() {

    self::create_project_taxonomy();
    flush_rewrite_rules();

    self::populate_project_tax();

    self::add_project_terms_to_cases();
  }

  public static function deactivate_project_filter_plugin() {

    $initial_terms = self::get_initial_terms();

    foreach ( $initial_terms as $key => $value ) {
      delete_term_meta( $key, 'project_term_id' );
    }
  }
}

class Project_Taxonomy_Processing {

  public function __construct() {
    add_action( 'created_term', array( $this, 'case_term_create' ), 10, 3 );
    add_action( 'edited_term', array( $this, 'case_term_edited' ), 10, 3 );
    add_filter( 'wp_insert_term_data', array( $this, 'before_insert_case_term' ), 10, 3 );
    add_filter( 'wp_update_term_data', array( $this, 'before_update_case_term' ), 10, 4 );
    add_action( 'pre_delete_term', array( $this, 'case_term_delete' ), 10, 2 );
    add_action( 'save_post_projects', array( $this, 'set_case_project_terms' ), 10, 3 );
  }

  private static function get_other_taxonomy( $taxonomy ) {
    return ( 'case_languages' === $taxonomy ) ? 'case_platforms' : 'case_languages';
  }

  private static function replace_special_chars_in_slug() {

  }

  public function set_case_project_terms( $post_id, $post ) {
    Project_Filter_Plugin::set_case_project_terms( $post_id, $post );
  }

  // On new case term create
  public function case_term_create( $term_id, $tt_id, $taxonomy ) {
    if ( in_array( $taxonomy, ['case_languages', 'case_platforms'] ) ) {
      $case_term = get_term( $term_id, $taxonomy );

      $project_term = get_term_by( 'slug', $case_term->slug, 'project' );
      if ( $project_term ) {
        $project_term_id = $project_term->term_id;
      } else {
        $project_term = wp_insert_term( $case_term->slug, 'project', array(
			'description' => '',
			'parent'      => 0,
			'slug'        => $case_term->slug
		) );
        $project_term_id = $project_term['term_id'];
      }

      update_term_meta( $term_id, 'project_term_id', $project_term_id );
    }
  }

	/**
	 * @param object $term
	 * @param string $taxonomy
	 * @param string $action
	 */
  public static function synchronize_term_posts( $term, $taxonomy, $action ) {
		global $post;
		$args = array(
			'post_type'      => 'projects',
			'posts_per_page' => -1,
			'post_status'    => 'any',
			'tax_query'      => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'slug',
					'terms'    => $term->slug,
				),
			),
		);
		$cases_query = new WP_Query( $args );
		if ( $cases_query->have_posts() ) {
			while ( $cases_query->have_posts() ) {
				$cases_query->the_post();
				if ( !has_term( $term->slug, 'project', $post ) && 'bind' == $action ) {
					wp_set_post_terms( $post->ID, [get_term_meta( $term->term_id, 'project_term_id', true )], 'project', true );
				}

				if ( has_term( $term->slug, 'project', $post ) && !has_term( $term->slug, self::get_other_taxonomy( $taxonomy ), $post ) && 'unbind' == $action ) {
					wp_remove_object_terms( $post->ID, $term->slug, 'project' );
				}
			}
		}
		wp_reset_postdata();
	}
  // On case term edit
  public function before_insert_case_term( $data, $taxonomy, $args ) {
	if ( in_array( $taxonomy, ['case_languages', 'case_platforms'] ) ) {
		if ( strpos( $args['name'], '+' ) ) {
			$data['slug'] = strtolower( preg_replace( '/(\+)/', '-plus', $args['name'] ) );
		}
		if ( strpos( $args['name'], '#' ) ) {
			$data['slug'] = strtolower( preg_replace( '/(\#)/', '-sharp', $args['name'] ) );
		}
		return $data;
	} else return $data;
  }

  public function before_update_case_term( $data, $term_id, $taxonomy, $args ) {
	  if ( in_array( $taxonomy, ['case_languages', 'case_platforms'] ) ) {
		  if ( strpos( $args['name'], '+' ) ) {
			  $data['slug'] = strtolower( preg_replace( '/(\+)/', '-plus', $args['name'] ) );
		  }
		  if ( strpos( $args['name'], '#' ) ) {
			  $data['slug'] = strtolower( preg_replace( '/(\#)/', '-sharp', $args['name'] ) );
		  }

		  return $data;
	  } else return $data;
  }

  public function case_term_edited( $term_id, $tt_id, $taxonomy ) {
    if ( in_array( $taxonomy, ['case_languages', 'case_platforms'] ) ) {
      $case_term = get_term( $term_id, $taxonomy );
			$other_case_term = get_term_by( 'slug', $case_term->slug, self::get_other_taxonomy( $taxonomy ) );

      $case_project_term_id = ( 0 == get_term_meta( $term_id, 'project_term_id', true ) ) ? '' : get_term_meta( $term_id, 'project_term_id', true );
      $case_project_term = get_term( $case_project_term_id, 'project' ); // Object or null

			if ( $other_case_term ) {
				update_term_meta( $term_id, 'project_term_id', $other_case_term->term_id, $case_project_term_id );

			} else {
				$new_project_term = wp_insert_term( $case_term->slug, 'project', array( 'slug' => $case_term->slug ) );
				if ( !is_wp_error( $new_project_term ) ) {
					update_term_meta( $term_id, 'project_term_id', $new_project_term['term_id'], $case_project_term_id );
				} else {
					$existing_project_term = get_term_by( 'slug', $case_term->slug, 'project' );
					update_term_meta( $term_id, 'project_term_id', $existing_project_term->term_id, $case_project_term_id );
				}
			}

			if ( $case_project_term && false == ( get_term_by( 'slug', $case_project_term->slug, 'case_languages' ) || get_term_by( 'slug', $case_project_term->slug, 'case_platforms' ) ) ) {
				wp_delete_term( $case_project_term_id, 'project' );
			}

			self::synchronize_term_posts( $case_term, $taxonomy, 'bind' );
    }
  }

  // On case term delete
  public function case_term_delete( $term_id, $taxonomy ) {
    if ( in_array( $taxonomy, ['case_languages', 'case_platforms'] ) ) {
      $case_term = get_term( $term_id, $taxonomy );
      $other_case_term = get_term_by( 'slug', $case_term->slug, self::get_other_taxonomy( $taxonomy ) );

      if ( false == $other_case_term ) {
        $project_term = get_term_by( 'slug', $case_term->slug, 'project' );
        wp_delete_term( $project_term->term_id, 'project' );
      } else {
      	self::synchronize_term_posts( $case_term, $taxonomy, 'unbind' );
			}
    }
  }
}


// Initialize plugin
$project_filter_plugin = new Project_Filter_Plugin();

register_activation_hook( __FILE__, array( 'Project_Filter_Plugin', 'activate_project_filter_plugin' ) );
register_deactivation_hook( __FILE__, array( 'Project_Filter_Plugin', 'deactivate_project_filter_plugin' ) );

$project_methods = new Project_Taxonomy_Processing();
