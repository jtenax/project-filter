# project-filter
Odoo task id: T38457
The plugin adds the Project taxonomy for Projects CPT, so that it can be used to aggregate both case_platforms and case_languages cases when visiting corresponding 'tag' links on single case page.